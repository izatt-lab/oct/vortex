Release Notes
=============

v0.4.3 -- 6/30/2022
-------------------

Changelog
+++++++++

Hardware
^^^^^^^^

- Add support for NI IMAQ cards with :class:`~vortex.acquire.ImaqAcquisition`
- Improve feature detection for Alazar cards
- Add calibration step to reduce ADC noise for select Alazar cards
- Fix dual edge sampling for Alazar cards
- Add strobe generation to engine with :data:`EngineConfig.strobes <vortex.engine.EngineConfig.strobes>`
- Transition to dynamically loaded hardware support modules

Documentation
^^^^^^^^^^^^^

- Reorganization of documentation with expansion for acquisition and processing components
- Parse docstrings from documentation and embed in compiled module
- Add galvo delay demo and :ref:`tuning tool <how-to/io-delay>`
- Add new demo for :ref:`saving data to disk <demo/acquire-to-disk>`
- Add tutorial for :ref:`UI development and scan switching <tutorial/display>`
- Improvements and bugfixes for demos

Build System
^^^^^^^^^^^^

- Drop builds for Python 3.6
- Add builds for CUDA 11.6
- Refactor CMake to support modular builds
- Fix issue that led to incorrect type stub output
- Add experimental support for Linux
- Explicitly set minimum required NumPy version
- Improve version detection and handling for Alazar and Vortex

Assorted
^^^^^^^^

- Fix issue where non-preloadable acquisition components produced incorrect startup sequences, leading to loss of synchronization
- Exceptions that cause premature engine shutdown are now propagated
- Add :data:`~vortex.engine.Block.StreamIndex.Counter` stream to support correct index bookkeeping in post-processing with :class:`~vortex.engine.CounterStackEndpoint` and related endpoints.
- Add endpoints for formatting and storing streams (e.g., :class:`~vortex.engine.GalvoActualStackHostTensorEndpoint`)
- Refactor spiral scan generation
- Fix bug that prevented change of repeated scan strategies
- Add multiple inactive segment generation policies
- Add CPU and GPU memory endpoints for spectra (e.g., :class:`~vortex.engine.SpectraStackDeviceTensorEndpointUInt16`)
- Assorted internal improvements and bugfixes

Migration Guide
+++++++++++++++

No migration required.

v0.4.2 -- 3/3/2022
------------------

Changelog
+++++++++

- Update database for newer Alazar cards
- Add support for tolerating unknown Alazar cards
- Add Python ``setup.py`` script for building *vortex*, including all dependencies with *vcpkg*
- Fix missing constructor for :class:`~vortex.engine.Source`
- Include imaging depth in :class:`~vortex.engine.Source`
- Build for Python 3.10
- Improvements and bugfixes for demos

Migration Guide
+++++++++++++++

Building
^^^^^^^^

*vortex* now includes its own *vcpkg* overlay for its dependencies in ``/.vcpkg``.
If you previously used this path for *vcpkg* (as suggested by earlier build guides), you will need to relocate it.

v0.4.1 -- 1/22/2022
-------------------

Changelog
+++++++++

- Add support for Alazar FFT acquisition with :class:`~vortex.acquire.AlazarFFTAcquisition` and :class:`~vortex.process.CopyProcessor`.
  See the :ref:`demo/alazar-fft` demo for an example.
- Move latency handling from formatter to engine.
- Add missing :func:`~vortex.scan.RasterScanConfig.to_segments` method for high-level scan classes.
- Add Python stubs for autocompletion and type checking.
- Add CMake presets.
- Add :class:`~vortex.format.PositionFormatExecutor`.
- Fix issues with :class:`~vortex.acquire.FileAcquisition`.
- Add ``sample`` to marker-associated callbacks.
- Assorted bug fixes and improvements.

Migration Guide
+++++++++++++++

Name Changes
^^^^^^^^^^^^

- ``Stack`` has replaced ``Cube`` in class names.  For example, ``CubeTensorEndpointX`` is now ``StackTensorEndpointX``.  Similarly, ``CubeFormatExecutor`` is now :class:`~vortex.format.StackFormatExecutor`.

IO Leading
^^^^^^^^^^

IO delays (e.g., for galvo response) are no longer handled in post-processing via the formatter. Instead, the engine now generates leading IO signals that cancel out the IO delay.
The delay in samples is passed via :meth:`~vortex.engine.EngineConfig.add_io` in the :class:`~vortex.engine.EngineConfig`.
Multiple IO delays are possible.
Change ``fc.stream_delay_samples = round(cfg.galvo_delay * ioc_out.samples_per_second)`` to ``ec.add_io(io_out, lead_samples=round(cfg.galvo_delay * ioc_out.samples_per_second))``.
