How To
======

These guides provide examples for on accomplishing common tasks with *vortex* and showcase the various options that *vortex* provides.

.. toctree::
   :maxdepth: 1

   how-to/custom-scan
   how-to/interleave
   how-to/bidirectional-repeated
   how-to/io-delay
   how-to/ascan-windowing
   how-to/additional-scan-channels
   how-to/alazar-fpga
   how-to/asynchronous-gpu
