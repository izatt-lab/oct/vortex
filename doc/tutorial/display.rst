.. _tutorial/display:

UI Integration
==============

.. warning::
    This document is under construction.

See `demo/tutorial/tutorial06.py <https://gitlab.oit.duke.edu/izatt-lab/oct/vortex/-/blob/develop/demo/tutorial/tutorial06.py>`_.

.. figure:: tutorial06.png

    User interface of the completed UI integration tutorial step.
