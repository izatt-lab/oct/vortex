.. _getting-started:

Getting Started
===============

*vortex* is available in Python and C++.
The Python bindings via a binary wheel is the fastest way to get started.

.. tip::
    See the :ref:`Python demo guide <demo/python>` for high-level step-by-step instructions to get started quickly.

.. _getting-started/python:

Python from Binary Wheel
------------------------

Pre-built binary wheels for recent Python and CUDA versions on Windows are published on the *vortex* `website`_.
Ensure that you install the wheel that matches the CUDA version for your system.
*vortex* uses the suffix ``-cudaXXY`` to denote a build for CUDA ``XX.Y``.
Installation of ``vortex-oct-tools`` is also recommended as it provides Python support classes for interacting with *vortex* and is required for certain demos.

.. _website: https://www.vortex-oct.dev/#releases

.. tab:: Windows

    .. code-block:: powershell

        > pip install vortex-oct-cuda112 vortex-oct-tools -f https://www.vortex-oct.dev/

.. tab:: Linux

    .. attention::

        Python binary wheels for Linux are not available at present, but you may :ref:`install from source <python-from-source>` using the command below after following the linked instructions.

    .. .. code-block:: bash

    ..     $ pip3 install vortex-oct vortex-oct-tools

Install Dependencies
^^^^^^^^^^^^^^^^^^^^

These binary wheels are compiled with support AlazarTech digitizers, CUDA-compatible GPUs, and National Instruments I/O and camera interface cards.
CUDA is a mandatory dependency for the binary wheels and must be installed for *vortex* to load.
All other hardware components are optional so you need install only the driver and runtime components for each of those specific devices that you plan to use.

.. tab:: Windows

    **Mandatory**

    -   `CUDA`_ runtime with version ``XX.Y`` for ``vortex-oct-cudaXXY``

    **Optional**

    -   `AlazarTech <AlazarTechWindows_>`_ drivers for the installed digitizer

        .. attention::

            The AlazarTech drivers are different from ATS-SDK, which is the software development kit only.
            ATS-SDK is required for building *vortex* and does not include the drivers.
            The AlazarTech drivers are required to run *vortex*.

    -   `NI DAQmx`_ runtime

    -   `NI IMAQ`_ runtime

.. tab:: Linux

    .. tip::

        See the :ref:`build guide <closed-source>` for help installing the Linux dependencies.

    **Mandatory**

    -   CUDA runtime with at least version ``XX.Y`` for ``vortex-oct-cudaXXY``

    **Optional**

    -   `AlazarTech <AlazarTechLinux_>`_ drivers for the installed digitizer

        .. attention::

            The AlazarTech drivers are different from ATS-SDK, which is the software development kit only.
            ATS-SDK is required for building *vortex* and does not include the drivers.
            The AlazarTech drivers are required to run *vortex*.

    -   `NI DAQmx`_ runtime

With the exception of CUDA runtime, *vortex* will detect the presence of these driver and runtime components and will activate the corresponding functionality.
See the :ref:`build-guide` for instructions on building Python bindings that match your hardware availability.

.. _`CUDA`: https://developer.nvidia.com/cuda-downloads
.. _`AlazarTechWindows`: https://www.alazartech.com/en/
.. _`AlazarTechLinux`: ftp://release@ftp.alazartech.com/outgoing/linux
.. _`NI DAQmx`: https://www.ni.com/en-us/support/downloads/drivers/download.ni-daqmx.html
.. _`NI IMAQ`: https://www.ni.com/en-us/support/downloads/drivers/download.vision-acquisition-software.html

Check Installation
^^^^^^^^^^^^^^^^^^

Check that *vortex* and its dependencies are installed correctly using the following command.
The command outputs ``OK``, the *vortex* version, and available features if *vortex* is correctly installed.

.. tab:: Windows

    .. code-block:: powershell

        > python -c "import vortex; print('OK', vortex.__version__, vortex.__feature__)"
        OK 0.4.3 ['reflexxes', 'cuda', 'hdf5', 'alazar', 'daqmx', 'simple']

.. tab:: Linux

    .. code-block:: bash

        $ python3 -c "import vortex; print('OK', vortex.__version__, vortex.__feature__)"
        OK 0.4.3 ['reflexxes', 'cuda', 'hdf5', 'alazar', 'daqmx', 'simple']

If an expected feature is absent from the list, ensure that you installed the correct driver or runtime component above.
For more detailed investigation, see the :ref:`debugging steps <debug-dynamic>`.
Once everything is installed, :ref:`try running a demo <demo/python>`.

.. _python-from-source:

Python from Source
------------------

The *vortex* setup script is capable of installing all open-source build dependencies using *vcpkg* and generating wheels for installation.
Building *vortex* from source is only recommended if the binary wheels are not suitable for your hardware.

.. _configure-build-environment:

Configure Build Environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. tab:: Windows

    The *vortex* setup script requires the Visual Studio Developer PowerShell (or Prompt) for building with *clang-cl*.
    Make sure to install `Clang/LLVM support for Visual Studio <https://docs.microsoft.com/en-us/cpp/build/clang-support-msbuild?view=msvc-160>`_.
    You can activate the Developer PowerShell from an existing PowerShell session as follows.

    .. code-block:: powershell

        > Import-Module C:"\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\Microsoft.VisualStudio.DevShell.dll"
        > Enter-VsDevShell -VsInstallPath "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community" -DevCmdArguments "-arch=x64"

.. tab:: Linux

    The *vortex* build script requires ``cmake``, ``ninja``, and ``clang`` for building.
    These and other required packages are available through the system package manager.

    .. code-block:: bash

        $ sudo apt install curl zip unzip tar pkg-config cmake ninja-build clang patchelf python3 python3-pip

Configure Features
^^^^^^^^^^^^^^^^^^

You can enable or disable features using the ``VORTEX_BUILD_FEATURES`` environment variable, which is a semicolon-delimited list of CMake variable definitions, especially those that :ref:`enable or disable features <dependency-control>`.
The setup script parses the feature specification and automatically installs the requires packages from *vcpkg*.
:ref:`Closed-source dependencies <closed-source>`, such as `CUDA`_ and `NI DAQmx`_, still require manual installation as for the binary wheel above.

.. tab:: Windows

    .. code-block:: powershell

        > $env:VORTEX_BUILD_FEATURES="WITH_ALAZAR=ON;WITH_CUDA=ON;WITH_DAQMX=ON;WITH_HDF5=ON;WITH_REFLEXXES=ON"

.. tab:: Linux

    .. code-block:: bash

        $ export VORTEX_BUILD_FEATURES="WITH_ALAZAR=ON;WITH_CUDA=ON;WITH_DAQMX=ON;WITH_HDF5=ON;WITH_REFLEXXES=ON"

If you do not want the setup script to install dependencies using *vcpkg*, define the environment variable ``VORTEX_DISABLE_AUTO_VCPKG`` or specify ``CMAKE_TOOLCHAIN_FILE`` or ``CMAKE_INSTALL_PREFIX`` as environment variables.

Build with Setup Script
^^^^^^^^^^^^^^^^^^^^^^^

*vortex* source distributions lack the ``-cudaXXY`` suffix and are thereby distinguished from the binary wheels.
You may clone the repository or `download the source <download-source_>`_, and then run the setup script.

.. tab:: Windows

    .. code-block:: powershell

        > git clone https://gitlab.oit.duke.edu/izatt-lab/oct/vortex.git
        > pip install --no-build-isolation ./vortex

.. tab:: Linux

    .. code-block:: bash

        $ git clone https://gitlab.oit.duke.edu/izatt-lab/oct/vortex.git
        $ pip3 install ./vortex

Alternatively, you may install directly from the repository.

.. tab:: Windows

    .. code-block:: powershell

        > pip install --no-build-isolation git+https://gitlab.oit.duke.edu/izatt-lab/oct/vortex.git

.. tab:: Linux

    .. code-block:: powershell

        $ pip3 install git+https://gitlab.oit.duke.edu/izatt-lab/oct/vortex.git

C++
---

A C++ installation provides full access to *vortex*'s features.
To get started quickly with a standard build configuration, see below.
For more advanced needs, see the :ref:`build-guide` to get your build system set up.

Build Dependencies
^^^^^^^^^^^^^^^^^^

In either case, clone the repository or `download the source <download-source>`_ first.

.. tab:: Windows

    .. code-block:: powershell

        > git clone https://gitlab.oit.duke.edu/izatt-lab/oct/vortex.git
        > git clone --depth 1 https://github.com/microsoft/vcpkg.git vortex/build/vcpkg
        > .\vortex\build\vcpkg\bootstrap-vcpkg.bat -disableMetrics
        > .\vortex\build\vcpkg\vcpkg.exe --triplet=x64-windows --overlay-ports=vortex\.vcpkg install fmt spdlog xtensor[tbb,xsimd] reflexxes cuda cub xtensor-python pybind11 hdf5[cpp]

.. tab:: Linux

    .. code-block:: bash

        $ git clone https://gitlab.oit.duke.edu/izatt-lab/oct/vortex.git
        $ git clone --depth 1 https://github.com/microsoft/vcpkg.git vortex/build/vcpkg
        $ ./vcpkg/bootstrap-vcpkg.sh -disableMetrics
        $ ./build/vcpkg/vcpkg --clean-after-build --overlay-ports=.vcpkg install \
            fmt spdlog xtensor[tbb,xsimd] reflexxes \
            cuda cub xtensor-python pybind11 hdf5[cpp]

Visual Studio
^^^^^^^^^^^^^

.. tab:: Windows

    Recent versions of Visual Studio (2019 or newer) and Visual Studio Code can open the *vortex* source root as a CMake project.
    Visual Studio will then use ``CMakePresets.json`` to configure and build the project.
    For more advanced needs, add your own ``CMakeUserPresets.json`` and consult the :ref:`build-guide`.
    Make sure to install `Clang/LLVM support for Visual Studio <https://docs.microsoft.com/en-us/cpp/build/clang-support-msbuild?view=msvc-160>`_.

.. tab:: Linux

    Visual Studio Code can open the *vortex* source root as a CMake project.
    It will then use ``CMakePresets.json`` to configure and build the project.
    For more advanced needs, add your own ``CMakeUserPresets.json`` and consult the :ref:`build-guide`.
    Make sure to install *clang* and *ninja* when `configuring your build environment <configure-build-environment>`_.

CMake
^^^^^

To use CMake directly, use any of the existing presets after `configuring your build environment <configure-build-environment>`_.
List the available presets using ``cmake --list-presets``.

.. tab:: Windows

    .. code-block:: powershell

        > cmake --list-presets
        Available configure presets:

        "clang-win-x64-debug"     - Clang x64 Windows Debug
        "clang-win-x64-release"   - Clang x64 Windows Release
        "clang-win-x64-unopt"     - Clang x64 Windows Unoptimized
        > cmake -S vortex -B vortex/build --preset clang-win-x64-release
        > cmake --build vortex/build

.. tab:: Linux

    .. code-block::

        $ cmake --list-presets
        Available configure presets:

        "clang-linux-x64-debug"     - Clang x64 Linux Debug
        "clang-linux-x64-release"   - Clang x64 Linux Release
        $ cmake -S vortex -B vortex/build --preset clang-linux-x64-release
        $ cmake --build vortex/build

For more advanced needs, see the :ref:`build-guide`.
