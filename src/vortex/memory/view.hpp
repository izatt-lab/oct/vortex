#pragma once

#include <xtensor/xadapt.hpp>
#include <xtensor/xstrides.hpp>
#include <xtensor/xstorage.hpp>

#include <fmt/format.h>

#include <vortex/core.hpp>
#include <vortex/util/variant.hpp>
#include <vortex/util/exception.hpp>

namespace vortex {

    namespace strategy {
        inline struct left_t {} left;
        inline struct right_t {} right;
        inline struct balance_t {} balance;
    }

    class incompatible_shape : public std::invalid_argument {
    public:
        using std::invalid_argument::invalid_argument;
    };

    namespace detail {

        template<typename T, typename shape_t, typename stride_t>
        class base_view_t {
        public:

            using element_t = std::decay_t<T>;

        protected:

            base_view_t(T* data)
                : _data(data) {}

        public:

            auto valid() const { return data() != nullptr; }

            auto data() const {
                return _data;
            }
            template<typename List>
            auto data(const List& idxs) const {
                return data(idxs.begin(), idxs.end());
            }
            auto data(const std::initializer_list<size_t>& idxs) const {
                return data(idxs.begin(), idxs.end());
            }
            template<typename Iterator>
            auto data(const Iterator& begin, const Iterator& end) const {
                return data() + offset(begin, end);
            }

            template<typename List>
            auto offset(const List& idxs) const {
                return offset(idxs.begin(), idxs.end());
            }
            auto offset(const std::initializer_list<size_t>& idxs) const {
                return offset(idxs.begin(), idxs.end());
            }
            template<typename Iterator>
            auto offset(const Iterator& begin, const Iterator& end) const {
                auto n = std::distance(begin, end);
                if (n > dimension()) {
                    throw traced<std::invalid_argument>(fmt::format("too many indices for shape [{}]: {}", shape_to_string(shape()), n));
                }

                ptrdiff_t o = 0;
                auto it_idx = begin;
                auto it_stride = _stride.begin();

                while(it_idx != end) {
                    o += *it_stride++ * *it_idx++;
                }

                return o;
            }

            auto count() const {
                if (_shape.size() == 0) {
                    return 0ULL;
                } else {
                    return std::accumulate(_shape.begin(), _shape.end(), 1ULL, std::multiplies());
                }
            }

            auto size_in_bytes() const {
                return count() * sizeof(T);
            }

            const auto& shape() const { return _shape; };
            auto shape(size_t idx) const { return _shape[idx]; }
            const auto& stride() const { return _stride; };
            auto stride(size_t idx) const { return _stride[idx]; }
            auto stride_in_bytes(size_t idx) const { return stride(idx) * sizeof(T); }
            auto stride_in_bytes() const {
                std::vector<size_t> s(dimension());
                for (size_t i = 0; i < dimension(); i++) {
                    s[i] = stride_in_bytes(i);
                }
                return s;
            }
            auto shape_and_stride() const {
                return std::make_tuple(shape(), stride());
            }

            auto dimension() const { return _shape.size(); }

        protected:

            // template<typename other_t>
            // auto _morph(other_t& dst, const strategy::left_t&) const {
            //     auto& src = *this;
            //     auto offset = std::abs<ptrdiff_t>(src.dimension() - dst.dimension());

            //     if (dst.dimension() < src.dimension()) {

            //         // source is larger than destination so ensure left has only trivial dimensions
            //         for (size_t i = 0; i < offset; i++) {
            //              if (src._shape[i] != 1) {
            //                  throw traced<std::invalid_argument>(fmt::format("source has too many non-trivial dimension on left: {} -> {}", shape_to_string(src._shape), shape_to_string(dst._shape)));
            //              }
            //         }

            //         // skip the trivial dimensions when copying
            //         std::copy(src._shape.begin() + offset, src._shape.end(), dst._shape.begin());
            //         std::copy(src._stride.begin() + offset, src._stride.end(), dst._stride.begin());

            //     } else if (dst.dimension() == src.dimension()) {

            //         // copy the actual shape and stride
            //         std::copy(src._shape.begin(), src._shape.end(), dst._shape.begin()());
            //         std::copy(src._stride.begin(), src._stride.end(), dst._stride.begin());

            //     } else {

            //         // pad on the left with trivial dimensions
            //         std::fill_n(dst._shape.begin(), offset, 1);
            //         std::fill_n(dst._stride.begin(), offset, 0);

            //         // copy the actual shape and stride after the new trivial dimensions
            //         std::copy(src._shape.begin(), src._shape.end(), dst._shape.begin() + offset);
            //         std::copy(src._stride.begin(), src._stride.end(), dst._stride.begin() + offset);
            //     }

            //     return dst;
            // }

            void _default_stride() {
                // NOTE: do not use xt::compute_strides(...) because it sets stride to 0 for singleton dimensions
                default_stride(_shape, _stride);
            }

            T* _data;
            shape_t _shape;
            stride_t _stride;

        };

        template<typename T, size_t N, typename static_t_, typename dynamic_t>
        class static_view_t : public base_view_t<T, std::array<size_t, N>, std::array<ptrdiff_t, N>> {
        protected:

            using base_t = base_view_t<T, std::array<size_t, N>, std::array<ptrdiff_t, N>>;
            template<size_t N2>
            using static_t = typename static_t_::template of_dimension<N2>;

        public:

            static_view_t()
                : base_t(nullptr) {}

            //template<typename S1, typename S2>
            //static_view_t(T* data, const S1& shape, const S2& stride)
            //    : base_view_t(data) {
            //    if (shape.size() != N || stride.size() != N) {
            //        throw traced<std::invalid_argument>(fmt::format("shape or stride dimension does not match view dimension: {} != {} || {} != {}", shape.size(), N, stride.size(), N));
            //    }

            //    _shape.assign(shape.begin(), shape.end());
            //    _stride.assign(stride.begin(), stride.end());
            //}
            template<typename S1>
            static_view_t(T* data, const S1& shape)
                : base_t(data) {
                if (shape.size() != N) {
                    throw traced<incompatible_shape>(fmt::format("shape of [{}] is not compatible with static view of dimension {}", shape_to_string(shape), N));
                }

                std::copy(shape.begin(), shape.end(), _shape.begin());
                _default_stride();
            }

            // convert from identically-sized dynamic view
            static_view_t(const dynamic_t& other)
                : static_view_t(other.data(), other.shape()) { }

            template<size_t N2, typename = std::enable_if_t<N2 < N>>
            auto index(std::array<size_t, N2> idx) const {
                return static_t<N2>(*static_cast<const static_t<N>*>(this), data(idx), tail<N2>(shape()));
            }
            auto range(size_t end) const {
                return range(0, end);
            }
            auto range(size_t start, size_t end) const {
                if (dimension() == 0 || start >= shape(0) || end > shape(0) || start >= end) {
                    throw traced<incompatible_shape>(fmt::format("cannot apply range [{}, {}) to first dimension with shape [{}]", start, end, shape_to_string(shape())));
                }

                auto shape = _shape;
                shape[0] = end - start;

                return static_t<N>(*static_cast<const static_t<N>*>(this), data({ start }), shape);
            }

            template<size_t N2>
            auto morph() const {
                return morph<N2>(strategy::left);
            }
            template<size_t N2, typename Strategy>
            auto morph(const Strategy& strategy) const {
                static_t<N2> dst;
                return _morph(dst, strategy);
            }

            bool is_contiguous() const { return true; }

            using base_t::shape, base_t::dimension, base_t::data;

        protected:

            using base_t::_data, base_t::_shape, base_t::_stride, base_t::_default_stride;

        };

        template<typename T, typename static_t_, typename dynamic_t>
        class dynamic_view_t : public base_view_t<T, xt::svector<size_t>, xt::svector<ptrdiff_t>> {
        protected:

            using base_t = base_view_t<T, xt::svector<size_t>, xt::svector<ptrdiff_t>>;
            template<size_t N2>
            using static_t = typename static_t_::template of_dimension<N2>;

        public:

            dynamic_view_t()
                : base_t(nullptr) {}

            //template<typename S1, typename S2>
            //dynamic_view_t(T* data, const S1& shape, const S2& stride)
            //    : base_view_t(data) {
            //    if (shape.size() != stride.size()) {
            //        throw traced<incompatible_shape>("shape and stride dimensions do not match");
            //    }

            //    _shape.assign(shape.begin(), shape.end());
            //    _stride.assign(stride.begin(), stride.end());
            //}
            template<typename S1>
            dynamic_view_t(T* data, const S1& shape)
                : base_t(data) {

                _shape.assign(shape.begin(), shape.end());
                _stride.resize(_shape.size());
                _default_stride();
            }

            // convert from static view to dynamic view
            template<size_t N>
            dynamic_view_t(const static_t<N>& other)
                : dynamic_view_t(other.data(), other.shape()) { }

            auto index(std::initializer_list<size_t> idx) const {
                if (idx.size() > dimension()) {
                    throw traced<incompatible_shape>(fmt::format("cannot index to ({}) with shape [{}]", shape_to_string(idx, ", "), shape_to_string(shape())));
                }

                auto s = _shape;
                s.erase(s.begin(), s.begin() + idx.size());

                // allow scalar views
                if (s.size() == 0) {
                    s.push_back(1);
                }

                return dynamic_t(*static_cast<const dynamic_t*>(this), data(idx), s);
            }
            auto range(size_t end) const {
                return range(0, end);
            }
            auto range(size_t start, size_t end) const {
                if (dimension() == 0 || start >= shape(0) || end > shape(0) || start >= end) {
                    throw traced<incompatible_shape>(fmt::format("cannot apply range [{}, {}) to first dimension with shape [{}]", start, end, shape_to_string(shape())));
                }

                auto shape = _shape;
                shape[0] = end - start;

                return dynamic_t(*static_cast<const dynamic_t*>(this), data({ start }), shape);
            }

            auto morph_right(size_t dim) const {
                auto& src = *this;
                auto offset = std::abs<ptrdiff_t>(src.dimension() - dim);

                auto dst_shape = shape();
                dst_shape.resize(dim);
                auto dst_stride = stride();
                dst_stride.resize(dim);

                if (dim < src.dimension()) {

                    // source is larger than destination so ensure right has only trivial dimensions
                    for (size_t i = dimension() - offset; i < dimension(); i++) {
                        if (src.shape(i) != 1) {
                            throw traced<incompatible_shape>(fmt::format("source has too many non-trivial dimensions on right: {} -> {}", shape_to_string(src.shape()), dim));
                        }
                    }

                    // skip the trivial dimensions when copying
                    std::copy(src.shape().begin(), src.shape().end() - offset, dst_shape.begin());
                    std::copy(src.stride().begin(), src.stride().end() - offset, dst_stride.begin());

                } else if (dim == src.dimension()) {

                    // copy the actual shape and stride
                    std::copy(src.shape().begin(), src.shape().end(), dst_shape.begin());
                    std::copy(src.stride().begin(), src.stride().end(), dst_stride.begin());

                } else {

                    // pad on the right with trivial dimensions
                    std::fill_n(dst_shape.end() - offset, offset, 1);
                    std::fill_n(dst_stride.end() - offset, offset, 0);

                    // copy the actual shape and stride after the new trivial dimensions
                    std::copy(src.shape().begin(), src.shape().end(), dst_shape.begin());
                    std::copy(src.stride().begin(), src.stride().end(), dst_stride.begin());
            }

                return dynamic_t(*static_cast<const dynamic_t*>(this), src.data(), std::move(dst_shape));
            }

            //void shrink_left(size_t desired_dimension) {
            //    while (dimension() > desired_dimension && _shape.front() == 1) {
            //        _shape.erase(_shape.begin());
            //        _stride.erase(_stride.begin)();
            //    }
            //    if (dimension != desired_dimension) {

            //    }
            //}
            //void shrink_right(size_t desired_dimension) {
            //
            //}
            //void grow_left(size_t desired_dimension) {
            //    while (dimension() < desired_dimension) {
            //        _shape.insert(_shape.begin(), 1);
            //        _stride.insert(_stride.begin(), 1);
            //    }
            //}
            //void grow_right(size_t desired_dimension) {
            //    while (dimension() < desired_dimension) {
            //        _shape.push_back(1);
            //        _stride.push_back(1);
            //    }
            //}

            bool is_contiguous() const { return true; }

            using base_t::shape, base_t::stride, base_t::dimension, base_t::data;

        protected:

            using base_t::_data, base_t::_shape, base_t::_stride, base_t::_default_stride;

        };

    }

    template<typename derived_t_>
    struct viewable {
        using derived_t = derived_t_;

        auto& derived_cast() {
            return *static_cast<derived_t*>(this);
        }
        const auto& derived_cast() const {
            return *static_cast<const derived_t*>(this);
        }
    };
    template<typename T>
    inline constexpr bool is_viewable = std::is_base_of_v<viewable<std::decay_t<T>>, std::decay_t<T>>;

    class unsupported_view : public std::invalid_argument {
    public:
        using invalid_argument::invalid_argument;
    };

#define VORTEX_VIEW_AS_IMPL(view_check, suffix) \
    template<typename F, typename... O, typename = typename std::enable_if_t<(sizeof...(O) >= 1)>> \
    void view_as_##suffix(F&& func, O&... obj) { \
        std::invoke([&](auto... buffers) { \
            if constexpr ((... && view_check<decltype(buffers)>)) { \
                std::invoke(func, std::move(buffers)...); \
            } else { \
                std::vector<std::string> type_names = { typeid(decltype(buffers)).name() ... }; \
                throw traced<unsupported_view>(fmt::format("({}) do not support " #suffix, join(type_names, ", "))); \
            } \
        }, std::move(view(obj))...); \
    } \
    template<typename F, typename... O> \
    void view_tuple_as_##suffix(F&& func, const std::tuple<O...>& tup) { \
        std::apply([&](const auto&... objs) { \
            view_as_##suffix([&](auto... buffers) { \
                std::invoke(func, std::move(std::make_tuple(buffers...))); \
            }, objs...); \
        }, tup); \
    }

}
