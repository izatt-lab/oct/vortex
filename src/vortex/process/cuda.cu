/** \rst

    CUDA kernels to support CUDA OCT processor

 \endrst */

// #include <cub/device/device_segmented_reduce.cuh>
// #include <cub/iterator/transform_input_iterator.cuh>

#include <vortex/driver/cuda/types.hpp>
#include <vortex/driver/cuda/runtime.hpp>
#include <vortex/driver/cuda/kernels.cuh>

// NOTE: no namespace shorthand here so NVCC can compile this file
namespace vortex {
    namespace process {
        namespace detail {

            //
            // resample
            //

            template<typename in_t, typename out_t, typename index_t, typename float_t>
            __global__
            static void _resample(size_t samples_per_record, size_t records_per_block, size_t samples_per_ascan, const index_t* before_index, const index_t* after_index, const float_t* before_weight, const float_t* after_weight, const in_t* in, out_t* out) {
                // A-scans run along the rows
                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;
                auto ascan_idx = blockIdx.y * blockDim.y + threadIdx.y;

                // check valid source coordinates
                if (sample_idx >= samples_per_ascan || ascan_idx >= records_per_block) {
                    return;
                }

                // look up bounding samples
                auto input_offset = ascan_idx * samples_per_record;
                auto& before = in[input_offset + before_index[sample_idx]];
                auto& after = in[input_offset + after_index[sample_idx]];

                // perform interpolation
                auto output_offset = ascan_idx * samples_per_ascan;
                out[output_offset + sample_idx] = cuda::kernel::round_clip_cast<out_t>(before_weight[sample_idx] * before + after_weight[sample_idx] * after);
            }

            template<typename in_t, typename out_t, typename index_t, typename float_t>
            void _resample_internal(const cuda::stream_t& stream, size_t samples_per_record, size_t records, size_t samples_per_ascan, const index_t* before_index, const index_t* after_index, const float_t* before_weight, const float_t* after_weight, const in_t* in, out_t* out) {
                auto threads = cuda::kernel::threads_from_shape(samples_per_ascan, records);
                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_ascan, records);

                _resample<<<blocks, threads, 0, stream.handle()>>>(samples_per_record, records, samples_per_ascan, before_index, after_index, before_weight, after_weight, in, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "resample kernel launch failed");
            }

            void resample(const cuda::stream_t& stream, size_t samples_per_record, size_t records, size_t samples_per_ascan, const uint16_t* before_index, const uint16_t* after_index, const float* before_weight, const float* after_weight, const uint16_t* in, float* out) {
                _resample_internal(stream, samples_per_record, records, samples_per_ascan, before_index, after_index, before_weight, after_weight, in, out);
            }
            void resample(const cuda::stream_t& stream, size_t samples_per_record, size_t records, size_t samples_per_ascan, const uint16_t* before_index, const uint16_t* after_index, const float* before_weight, const float* after_weight, const float* in, float* out) {
                _resample_internal(stream, samples_per_record, records, samples_per_ascan, before_index, after_index, before_weight, after_weight, in, out);
            }

            //
            // remove average
            //

            struct transpose_op_t {
                __host__ __device__
                    transpose_op_t(size_t rows, size_t cols) : _rows(rows), _cols(cols) {

                }

                __host__ __device__ __forceinline__
                    size_t operator()(const size_t& i) const {
                    return (i % _rows) * _cols + (i / _rows);
                }

                size_t _rows, _cols;
            };

            __global__
            static void _compute_average(size_t samples_per_record, const uint16_t* average_record_buffer, size_t average_record_count, float* average_record) {
                // A-scans run along the rows
                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;

                // check valid source coordinates
                if (sample_idx >= samples_per_record) {
                    return;
                }

                // perform averaging
                float out = 0.0f;
                for (auto record_idx = 0; record_idx < average_record_count; record_idx++) {
                    auto offset = record_idx * samples_per_record + sample_idx;
                    out += average_record_buffer[offset];
                }

                // store result
                average_record[sample_idx] = out / average_record_count;
            }

//            __global__
//                static void _normalize_average(size_t samples_per_record, size_t average_record_count, float* average_record) {
//                // A-scans run along the rows
//                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;
//
//                // check valid source coordinates
//                if (sample_idx >= samples_per_record) {
//                    return;
//                }
//
//                // normalize
//                average_record[sample_idx] /= average_record_count;
//            }
//
//            size_t compute_average_internal_storage_size(size_t samples_per_record, const uint16_t* average_record_buffer, size_t average_record_count, float* average_record) {
//                transpose_op_t transpose_op(average_record_count, samples_per_record);
//                cub::TransformInputIterator<size_t, transpose_op_t, const uint16_t*> input_it(average_record_buffer, transpose_op);
//
//                std::vector<size_t> offsets;
//                offsets.resize(samples_per_record + 1);
//                for (size_t i = 0; i < offsets.size(); i++) {
//                    offsets[i] = i * average_record_count;
//                }
//
//                size_t internal_storage_size_in_bytes;
//                cub::DeviceSegmentedReduce::Sum(nullptr, internal_storage_size_in_bytes, input_it, average_record, samples_per_record, offsets.data(), offsets.data() + 1);
//
//                return internal_storage_size_in_bytes;
//            }
//            void compute_average(const cuda::stream_t& stream, const uint8_t* internal_storage, size_t samples_per_record, const uint16_t* average_record_buffer, size_t average_record_count, float* average_record) {
//                cudaError_t error;
//
//                transpose_op_t transpose_op(average_record_count, samples_per_record);
//                cub::TransformInputIterator<size_t, transpose_op_t, const uint16_t*> input_it(average_record_buffer, transpose_op);
//
//                std::vector<size_t> offsets;
//                offsets.resize(samples_per_record + 1);
//                for (size_t i = 0; i < offsets.size(); i++) {
//                    offsets[i] = i * average_record_count;
//                }
//
//                // compute the sum
//                size_t internal_storage_size_in_bytes = internal_storage.size_in_bytes();
//                cub::DeviceSegmentedReduce::Sum((void*)internal_storage, internal_storage_size_in_bytes, input_it, average_record, samples_per_record, offsets.data(), offsets.data() + 1, stream.handle(), true);
//#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
//                cudaDeviceSynchronize();
//#endif
//                error = cudaGetLastError();
//                cuda::detail::handle_error(error, "sum kernel launch failed");
//
//                auto threads = cuda::kernel::threads_from_shape(samples_per_record);
//                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_record);
//
//                _normalize_average<<<blocks, threads, 0, stream.handle()>>>(samples_per_record, average_record_count, average_record);
//#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
//                cudaDeviceSynchronize();
//#endif
//                error = cudaGetLastError();
//                cuda::detail::handle_error(error, "normalize kernel launch failed");
//            }
            void compute_average(const cuda::stream_t& stream, size_t samples_per_record, const uint16_t* average_record_buffer, size_t average_record_count, float* average_record) {
                auto threads = cuda::kernel::threads_from_shape(samples_per_record);
                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_record);

                _compute_average<<<blocks, threads, 0, stream.handle()>>>(samples_per_record, average_record_buffer, average_record_count, average_record);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "compute average kernel launch failed");
            }

            template<typename in_t>
            __global__
            static void _subtract_average(size_t samples_per_record, size_t records_per_block, const float* average_record, const in_t* in, float* out) {
                // A-scans run along the rows
                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;
                auto record_idx = blockIdx.y * blockDim.y + threadIdx.y;

                // check valid source coordinates
                if (sample_idx >= samples_per_record || record_idx >= records_per_block) {
                    return;
                }

                // perform average subtraction
                auto offset = record_idx * samples_per_record + sample_idx;
                out[offset] = in[offset] - average_record[sample_idx];
            }

            template<typename in_t>
            static void _subtract_average_internal(const cuda::stream_t& stream, size_t samples_per_record, size_t records, const uint16_t* average_record_buffer, size_t average_record_count, const float* average_record, const in_t* in, float* out) {
                auto threads = cuda::kernel::threads_from_shape(samples_per_record, records);
                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_record, records);

                _subtract_average<<<blocks, threads, 0, stream.handle()>>>(samples_per_record, records, average_record, in, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "subtract average kernel launch failed");
            }
            void subtract_average(const cuda::stream_t& stream, size_t samples_per_record, size_t records, const uint16_t* average_record_buffer, size_t average_record_count, const float* average_record, const uint16_t* in, float* out) {
                _subtract_average_internal(stream, samples_per_record, records, average_record_buffer, average_record_count, average_record, in, out);
            }

            //
            // complex filter
            //

            template<typename in_t>
            __global__
            static void _complex_filter(size_t samples_per_ascan, size_t ascans_per_block, const in_t* in, const cuFloatComplex* filter, cuFloatComplex* out) {
                // A-scans run along the rows
                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;
                auto ascan_idx = blockIdx.y * blockDim.y + threadIdx.y;

                // check valid source coordinates
                if (sample_idx >= samples_per_ascan || ascan_idx >= ascans_per_block) {
                    return;
                }

                // perform filtering
                auto offset = ascan_idx * samples_per_ascan + sample_idx;
                out[offset].x = filter[sample_idx].x * in[offset];
                out[offset].y = filter[sample_idx].y * in[offset];
            }

            void complex_filter(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, const uint16_t* in, const cuFloatComplex* filter, cuFloatComplex* out) {
                auto threads = cuda::kernel::threads_from_shape(samples_per_ascan, ascans);
                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_ascan, ascans);

                _complex_filter<<<blocks, threads, 0, stream.handle()>>>(samples_per_ascan, ascans, in, filter, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "complex filter kernel launch failed");
            }
            void complex_filter(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, const float* in, const cuFloatComplex* filter, cuFloatComplex* out) {
                auto threads = cuda::kernel::threads_from_shape(samples_per_ascan, ascans);
                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_ascan, ascans);

                _complex_filter<<<blocks, threads, 0, stream.handle()>>>(samples_per_ascan, ascans, in, filter, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "complex filter kernel launch failed");
            }

            //
            // cast
            //

            template<typename in_t>
            __global__
            static void _cast(size_t samples_per_ascan, size_t ascans_per_block, const in_t* in, cuFloatComplex* out) {
                // A-scans run along the rows
                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;
                auto ascan_idx = blockIdx.y * blockDim.y + threadIdx.y;

                // check valid source coordinates
                if (sample_idx >= samples_per_ascan || ascan_idx >= ascans_per_block) {
                    return;
                }

                // perform casting
                auto offset = ascan_idx * samples_per_ascan + sample_idx;
                out[offset].x = float(in[offset]);
                out[offset].y = 0.0f;
            }

            template<typename in_t>
            static void _cast_internal(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, const in_t* in, cuFloatComplex* out) {
                auto threads = cuda::kernel::threads_from_shape(samples_per_ascan, ascans);
                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_ascan, ascans);

                _cast<<<blocks, threads, 0, stream.handle()>>>(samples_per_ascan, ascans, in, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "cast kernel launch failed");
            }
            void cast(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, const uint16_t* in, cuFloatComplex* out) {
                _cast_internal(stream, samples_per_ascan, ascans, in, out);
            }
            void cast(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, const float* in, cuFloatComplex* out) {
                _cast_internal(stream, samples_per_ascan, ascans, in, out);
            }
            void cast(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, const cuFloatComplex* in, cuFloatComplex* out) {
                // NOTE: only copy if in and out are different buffers
                if (&in != &out) {
                    cuda::detail::memcpy(out, in, samples_per_ascan * ascans, cudaMemcpyDeviceToDevice, &stream);
                }
            }

            //
            // log abs normalize
            //

            template<typename in_t, typename out_t, typename factor_t>
            __global__
            static void _abs2_normalize(size_t samples_per_ascan, size_t ascans_per_block, factor_t factor, const in_t* in, out_t* out) {
                // A-scans run along the rows
                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;
                auto ascan_idx = blockIdx.y * blockDim.y + threadIdx.y;

                // check valid source coordinates
                if (sample_idx >= samples_per_ascan || ascan_idx >= ascans_per_block) {
                    return;
                }

                // perform  abs normalize
                auto offset = ascan_idx * samples_per_ascan + sample_idx;
                out[offset] = cuda::kernel::round_clip_cast<out_t>(cuda::kernel::sqr(factor * cuda::kernel::abs(in[offset])));
            }

            template<typename in_t, typename out_t, typename factor_t>
            __global__
            static void _log10_abs2_normalize(size_t samples_per_ascan, size_t ascans_per_block, factor_t factor, const in_t* in, out_t* out) {
                // A-scans run along the rows
                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;
                auto ascan_idx = blockIdx.y * blockDim.y + threadIdx.y;

                // check valid source coordinates
                if (sample_idx >= samples_per_ascan || ascan_idx >= ascans_per_block) {
                    return;
                }

                // perform log abs2 normalize
                auto offset = ascan_idx * samples_per_ascan + sample_idx;
                out[offset] = cuda::kernel::round_clip_cast<out_t>(20 * log10(factor * cuda::kernel::abs(in[offset])));
            }

            template<typename in_t, typename out_t, typename factor_t>
            static void _abs2_normalize_internal(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, factor_t factor, const in_t* in, out_t* out) {
                auto threads = cuda::kernel::threads_from_shape(samples_per_ascan, ascans);
                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_ascan, ascans);

                _abs2_normalize<<<blocks, threads, 0, stream.handle()>>>(samples_per_ascan, ascans, factor, in, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "abs2 normalize kernel launch failed");
            }

            template<typename in_t, typename out_t, typename factor_t>
            static void _log10_abs2_normalize_internal(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, factor_t factor, const in_t* in, out_t* out) {
                auto threads = cuda::kernel::threads_from_shape(samples_per_ascan, ascans);
                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_ascan, ascans);

                _log10_abs2_normalize<<<blocks, threads, 0, stream.handle()>>>(samples_per_ascan, ascans, factor, in, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "log10 abs2 normalize kernel launch failed");
            }

#define _DECLARE(factor_t, in_t, out_t) \
    void abs2_normalize(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, factor_t factor, const in_t* in, out_t* out) { \
        _abs2_normalize_internal(stream, samples_per_ascan, ascans, factor, in, out); \
    } \
    void log10_abs2_normalize(const cuda::stream_t& stream, size_t samples_per_ascan, size_t ascans, factor_t factor, const in_t* in, out_t* out) { \
        _log10_abs2_normalize_internal(stream, samples_per_ascan, ascans, factor, in, out); \
    }

            _DECLARE(float, uint16_t, float);
            _DECLARE(float, float, float);
            _DECLARE(float, cuFloatComplex, float);
            _DECLARE(float, uint16_t, int8_t);
            _DECLARE(float, float, int8_t);
            _DECLARE(float, cuFloatComplex, int8_t);
#undef _DECLARE

            // NOTE: not yet ready to support double for the other functions but there is no reason it cannot be done
            //_DECLARE(double, uint16_t, double);
            //_DECLARE(double, double, double);
            //_DECLARE(double, cuDoubleComplex, double);
            //_DECLARE(double, uint16_t, int8_t);
            //_DECLARE(double, double, int8_t);
            //_DECLARE(double, cuDoubleComplex, int8_t);

        }
    }
}
