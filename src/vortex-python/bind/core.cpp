#include <vortex-python/bind/common.hpp>

#include <mutex>

#include <spdlog/spdlog.h>
#include <spdlog/async.h>
#include <spdlog/sinks/base_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <vortex/core.hpp>
#include <vortex/driver/motion.hpp>

template<typename T>
static auto _to_string_view(T& o) {
    return std::string_view(o.data(), o.size());
}

class python_sink : public spdlog::sinks::base_sink<std::mutex> {
protected:
    void sink_it_(const spdlog::details::log_msg& msg) override {
        py::gil_scoped_acquire gil;

        // get the appropriate Python logger
        auto name = _to_string_view(msg.logger_name);
        auto& log = _loggers[name];
        if (!log) {
            auto logging = py::module::import("logging");
            log = logging.attr("getLogger")(name).attr("log");
        }

        // forward on to Python, mapping the level appropriately
        log(msg.level * 10, _to_string_view(msg.payload));
    }

    void flush_() override {
        // no flush capability for a Python logger, only handlers
    }

    std::unordered_map<std::string_view, py::object> _loggers;
};

template<typename T>
static auto bind_range(py::module& m) {
    using C = vortex::range_t<T>;
    CLS_VAL(Range);

    c.def(py::init());
    c.def(py::init([](T min, T max) -> C { return { {min, max} }; }));
    c.def(py::self == py::self);

    RO_ACC(length);

    RW_ACC(min);
    RW_ACC(max);

    SFXN(symmetric);
    FXN(contains);

    SHALLOW_COPY();

    c.def("__repr__", [](const C& o) {
        return fmt::format("Range({}, {})", o.min(), o.max());
    });

    //c.def("__array__", [](const C & o) {
    //    return std::vector<T>{ o.min(), o.max() };
    //});

    return c;
}

#if defined(NDEBUG)
#   define make_logger(sink, name) spdlog::create_async<sink>(name)
#else
#   define make_logger(sink, name) spdlog::create<sink>(name)
#endif

static auto get_console_logger(const std::string& name, int level) {
    auto logger = spdlog::get(name);
    if (logger) {
        return logger;
    }

    logger = make_logger(spdlog::sinks::stderr_color_sink_mt, name);

    logger->set_pattern("[%d-%b-%Y %H:%M:%S.%f] %-10n %^(%L) %v%$");
    logger->set_level(spdlog::level::level_enum(level));

    return logger;
}

static auto get_python_logger(std::string name, const std::string& scope) {
    name = scope + "." + name;

    auto logger = spdlog::get(name);
    if (logger) {
        return logger;
    }

    logger = make_logger(python_sink, name);

    // ensure all messages get passed to Python
    // TODO: figure out why trace level messages are not handled on Python side
    logger->set_level(spdlog::level::trace);

    return logger;
}

void bind_core(py::module& m) {
    bind_range<double>(m);

    py::class_<spdlog::logger, std::shared_ptr<spdlog::logger>>(m, "Logger");

    m.def("get_console_logger", &get_console_logger, "name"_a, "level"_a = int(spdlog::level::info));
    m.def("get_python_logger", &get_python_logger, "name"_a, "scope"_a = "vortex");
}
