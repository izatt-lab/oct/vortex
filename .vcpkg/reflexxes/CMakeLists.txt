cmake_minimum_required(VERSION 3.9)

project(Reflexxes VERSION 1.2.7 LANGUAGES CXX)

file(GLOB REFLEXXES_TYPEII_SOURCES "src/TypeIIRML/*.cpp")
file(GLOB REFLEXXES_TYPEII_HEADERS "include/*.h")

add_library(TypeII ${REFLEXXES_TYPEII_SOURCES} ${REFLEXXES_TYPEII_HEADERS})
set_target_properties(TypeII PROPERTIES OUTPUT_NAME Reflexxes_TypeII)

target_include_directories(TypeII PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
    $<INSTALL_INTERFACE:include/Reflexxes>
)

install(
    TARGETS TypeII
    EXPORT ReflexxesConfig
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    INCLUDES DESTINATION include
)

install(
    EXPORT ReflexxesConfig
    FILE ReflexxesConfig.cmake
    NAMESPACE Reflexxes::
    DESTINATION share/reflexxes
)

install (FILES ${REFLEXXES_TYPEII_HEADERS} DESTINATION include/Reflexxes CONFIGURATIONS Release)
