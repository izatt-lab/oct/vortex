# skip vcpkg setup if already present
if(Test-Path build/vcpkg) {
    Write-Host "-- Detected vcpkg in cache"
} else {
    git clone https://github.com/microsoft/vcpkg.git build/vcpkg
    if($LastExitCode -ne 0) { exit $LastExitCode }
    build/vcpkg/bootstrap-vcpkg.bat -disableMetrics
    if($LastExitCode -ne 0) { exit $LastExitCode }
}

# ensure vcpkg is up to date
$Deps = @(Get-Content .gitlab-ci/vcpkg-deps.txt)
Write-Host "-- Installing $Deps"
build/vcpkg/vcpkg.exe --triplet=$TRIPLET --clean-after-build --overlay-ports=.vcpkg install @Deps
if($LastExitCode -ne 0) { exit $LastExitCode }
