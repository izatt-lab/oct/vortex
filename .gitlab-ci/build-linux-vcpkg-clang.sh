set -e

echo "-- Setting up vcpkg"
git clone https://github.com/microsoft/vcpkg.git build/vcpkg
build/vcpkg/bootstrap-vcpkg.sh -disableMetrics

# ensure vcpkg is up to date
DEPS = $(cat .gitlab-ci/vcpkg-deps.txt | xargs echo)
echo "-- Installing $DEPS"
build/vcpkg/vcpkg --triplet=$TRIPLET --clean-after-build --overlay-ports=.vcpkg install $DEPS

# locate Python and log version
PYTHON_ROOT = "/usr/local/python${PY_VER}"
PYTHON = "${PYTHON}/bin/python${PY_VER}"
echo "-- Using ${PYTHON} for CMake Python3"
${PYTHON} -c "import sys; print(sys.executable, sys.version);"

# create virtual environment
echo "-- Creating virtual environment"
PYTHON_ROOT = "build/venv"
${PYTHON} -m venv ${PYTHON_ROOT}
.ve/bin/activate

# remap python map to virtual environment
PYTHON = "${PYTHON}/bin/python${PY_VER}"

# install Python build dependencies
echo "-- Installing build requirements"
python -m pip install oldest-supported-numpy -r requirements.txt

# locate NVCC and log version
CUDA_ROOT = "/usr/local/cuda-${CUDA_VER}"
NVCC =  "$CUDA_ROOT/bin/nvcc"
echo "Using $nvcc for CMake CUDA"
${NVCC} --version

# build
cmake --preset clang-linux-x64-release -S . -B build/vortex \
    -DCUDAToolkit_ROOT:PATH=${CUDA_ROOT} \
    -DPython3_ROOT_DIR:PATH=${PYTHON_ROOT} \
    -DENABLE_INSTALL_PYTHON_WHEEL:BOOL=OFF

cmake --build build/vortex

# quick build check
${PYTHON} -c "import vortex; print('OK', vortex.__version__, vortex.__features__)"

# log wheel output
ls -lh build/vortex/dist/*.whl
