# setup conda (ref: https://stackoverflow.com/questions/57754356/activating-conda-environment-during-gitlab-ci)
conda init powershell | Out-Null
if($LastExitCode -ne 0) { exit $LastExitCode }
if (Test-Path $PROFILE.CurrentUserAllHosts) {
    & $PROFILE.CurrentUserAllHosts
}

# setup conda environment
conda create --name $VENV -q -y
if($LastExitCode -ne 0) { exit $LastExitCode }
conda activate $VENV
if($LastExitCode -ne 0) { exit $LastExitCode }
pip install twine
if($LastExitCode -ne 0) { exit $LastExitCode }

# log available wheels
Get-ChildItem build/vortex/dist

# upload wheels, using TWINE_USERNAME and TWINE_PASSWORD from environment
python -m twine upload --repository-url $REPOSITORY build/vortex/dist/*.whl
if($LastExitCode -ne 0) { exit $LastExitCode }
