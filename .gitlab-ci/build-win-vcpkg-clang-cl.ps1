# setup conda (ref: https://stackoverflow.com/questions/57754356/activating-conda-environment-during-gitlab-ci)
conda init powershell | Out-Null
if($LastExitCode -ne 0) { exit $LastExitCode }
if (Test-Path $PROFILE.CurrentUserAllHosts) {
    & $PROFILE.CurrentUserAllHosts
}

$CUDA_VER_UNDERSCORE = $CUDA_VER -Replace '\.', '_'

# setup conda environment
Write-Host "-- Creating conda environment $VENV with python=$PY_VER"
conda create --name $VENV python=$PY_VER -q -y
if($LastExitCode -ne 0) { exit $LastExitCode }
conda activate $VENV
if($LastExitCode -ne 0) { exit $LastExitCode }
conda info

# locate Python and log version
$Python = (Get-Command python).Path
Write-Host "-- Using $Python for CMake Python3"
& $Python -c "import sys; print(sys.executable, sys.version);"
if($LastExitCode -ne 0) { exit $LastExitCode }

# install Python build dependencies
Write-Host "-- Installing build requirements"
& $Python -m pip install oldest-supported-numpy -r requirements.txt

# locate NVCC and log version
$CUDA_PATH = (Get-Item env:"CUDA_PATH_V$CUDA_VER_UNDERSCORE").Value
$nvcc =  "$CUDA_PATH/bin/nvcc.exe"
Write-Host "Using $nvcc for CMake CUDA"
& "$nvcc" --version
if($LastExitCode -ne 0) { exit $LastExitCode }

# configure build
$env:CC = "clang-cl"
$env:CXX = "clang-cl"
$env:CXXFLAGS = "-m64 -fdiagnostics-absolute-paths -Wno-unused-command-line-argument -Wno-deprecated-volatile -Wno-deprecated-declarations -Wunknown-pragmas /DTBB_SUPPRESS_DEPRECATED_MESSAGES"
$env:CUDAFLAGS = "--expt-relaxed-constexpr -lineinfo -Xcudafe --diag_suppress=base_class_has_different_dll_interface"

# activate build tool prompt (ref: https://developercommunity.visualstudio.com/t/x64-developer-powershell-for-vs-2019/943058)
Import-Module C:"\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\Microsoft.VisualStudio.DevShell.dll"
Enter-VsDevShell -VsInstallPath "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community" -DevCmdArguments "-arch=x64"

# configure
# 'Get-ChildItem env: | Format-Table -Wrap -AutoSize'
cmake -S . -B build/vortex -G Ninja `
    -DCMAKE_BUILD_TYPE:STRING=Release `
    -DCMAKE_CUDA_COMPILER:FILEPATH="$nvcc" `
    -DCMAKE_CUDA_STANDARD:STRING="14" `
    -DPython3_EXECUTABLE:FILEPATH="$Python" `
    -DCMAKE_TOOLCHAIN_FILE:FILEPATH="$PWD/build/vcpkg/scripts/buildsystems/vcpkg.cmake" `
    -DENABLE_INSTALL_PYTHON_WHEEL:BOOL=OFF `
    -DWITH_IMAQ:BOOL=ON
if($LastExitCode -ne 0) { exit $LastExitCode }

# build
cmake --build build/vortex -- -v
if($LastExitCode -ne 0) { exit $LastExitCode }

# log wheel output
Get-ChildItem build/vortex/dist/*.whl
