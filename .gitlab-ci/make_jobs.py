from copy import deepcopy

class V:
    def __init__(self, major, minor):
        self.major = major
        self.minor = minor

    @property
    def dot(self) -> str:
        return f'{self.major}.{self.minor}'
    @property
    def nodot(self) -> str:
        return f'{self.major}{self.minor}'

def Vs(l: list):
    return [V(*o.split('.')) for o in l]

def add_jobs(jobs: dict, os: V, py: V, cuda: V) -> None:
    vs = {
        'PY_VER': py.nodot,
        'PY_VER_DOT': py.dot,
        'CUDA_VER': cuda.nodot,
        'CUDA_VER_DOT': cuda.dot
    }

    tags = [ f'py{py.nodot}', f'cuda{cuda.nodot}']
    build = '-'.join(['build', os] + tags)
    test = '-'.join(['test', os] + tags)

    jobs.update({
        build: {
            'tags': tags,
            'variables': vs,
            'extends': [f'.build-{os}'],
        },
        test: {
            'tags': tags,
            'variables': vs,
            'extends': [f'.test-{os}'],
            'needs': [build],
        }
    })

def _get_parents(jobs: dict, job: dict) -> list:
    extends = job.get('extends', [])
    extends = sum([_get_parents(jobs, jobs[e]) for e in extends], []) + extends
    return extends

def merge_extends(bases: dict, job: dict) -> None:
    for name in reversed(_get_parents(bases, job)):
        base = bases[name]

        for (k, v) in base.items():
            if k in job:
                if isinstance(v, list):
                    # prepend
                    job[k] = v + job[k]
                elif isinstance(v, dict):
                    # reverse update
                    d = v.copy()
                    d.update(job[k])
                    job[k] = d
                else:
                    raise ValueError(f'unsupported: {k} {v}')
            else:
                # assign directly
                job[k] = deepcopy(v)

    # no longer extends
    try:
        del job['extends']
    except KeyError:
        pass

oses = ['windows', 'linux']
pythons = Vs(['3.7', '3.8', '3.9', '3.10'])
cudas = Vs(['10.2', '11.2', '11.6'])

if __name__ == '__main__':
    import sys
    from pathlib import Path

    jobs = {}

    from itertools import product
    for (os, py, cuda) in product(oses, pythons, cudas):
        add_jobs(jobs, os, py, cuda)

    base_text = Path(sys.argv[1]).read_text()

    import yaml
    bases = yaml.safe_load(base_text)

    for job in jobs.values():
        if isinstance(job, dict):
            merge_extends(bases, job)
    for base in bases.values():
        if isinstance(base, dict):
            merge_extends(bases, base)

    # remove bases
    for k in list(bases):
        if k.startswith('.'):
            del bases[k]

    print(yaml.safe_dump(bases, indent=4, width=99999))
    print(yaml.safe_dump(jobs, indent=4, width=99999))
