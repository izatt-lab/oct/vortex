# setup conda (ref: https://stackoverflow.com/questions/57754356/activating-conda-environment-during-gitlab-ci)
conda init powershell | Out-Null
if($LastExitCode -ne 0) { exit $LastExitCode }
if (Test-Path $PROFILE.CurrentUserAllHosts) {
    & $PROFILE.CurrentUserAllHosts
}

$PY_VER_NODOT = $PY_VER -Replace '\.', ''
$CUDA_VER_NODOT = $CUDA_VER -Replace '\.', ''

# setup conda environment
Write-Host "-- Creating conda environment $VENV with Python $PY_VER"
conda create --name $VENV python=$PY_VER -q -y
if($LastExitCode -ne 0) { exit $LastExitCode }
conda activate $VENV
if($LastExitCode -ne 0) { exit $LastExitCode }
conda info

# give CuPy the correct DLL path
# NOTE: the 'bin' suffix is not added when conda is detected so add it here
$env:CUDA_PATH="C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v$CUDA_VER/bin"
$env:PATH="$env:CUDA_PATH;$env:PATH"

# locate Python
$Python = (Get-Command python).Path
Write-Host "-- Using $Python for Python3"
& $Python -c "import sys; print(sys.executable, sys.version);"
if($LastExitCode -ne 0) { exit $LastExitCode }

# install dependencies
Write-Host "-- Installing test requirements"
& $Python -m pip install cupy-cuda${CUDA_VER_NODOT} -r .gitlab-ci/requirements-test.txt
if($LastExitCode -ne 0) { exit $LastExitCode }

# install Vortex
Get-ChildItem build/vortex/dist
$Vortex = Join-Path build/vortex/dist "vortex_oct_cuda${CUDA_VER_NODOT}-*-*${PY_VER_NODOT}*-*${PY_VER_NODOT}*-win_${ARCH}.whl" -Resolve
if(!($Vortex)) {
    Write-Host "Could not find Vortex wheel"
    exit 1
}
Write-Host "-- Installing Vortex from $Vortex"
& $Python -m pip install $Vortex
if($LastExitCode -ne 0) { exit $LastExitCode }

# run tests
Write-Host "-- Running tests"
& $Python -m pytest --instafail test/offline --junitxml=report-py${PY_VER}-cuda${CUDA_VER}.xml
if($LastExitCode -ne 0 -And !($FORCE_TEST_PASS)) { exit $LastExitCode }
