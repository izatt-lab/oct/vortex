import pytest

def _round_clip_cast(xp, data, dtype):
    info = xp.iinfo(dtype)
    return data.round().clip(info.min, info.max).astype(dtype)

@pytest.fixture
def cpu_processor():
    try:
        from vortex.process import CPUProcessor
    except ImportError:
        pytest.skip('CPU processor not supported')

    import numpy
    return (CPUProcessor(), numpy)

@pytest.fixture
def cuda_processor():
    try:
        from vortex.process import CUDAProcessor
    except ImportError:
        pytest.skip('CUDA processor not supported')

    cupy = pytest.importorskip('cupy')

    if int(cupy.cuda.Device().compute_capability) < 35:
        pytest.skip('GPU compute capability is insufficient')

    return (CUDAProcessor(), cupy)

@pytest.fixture
def samples_per_record():
    return 1376

@pytest.fixture
def ascans_per_block():
    return 250

@pytest.fixture(params=['cpu_processor', 'cuda_processor'])
def scenario(request, samples_per_record, ascans_per_block):
    (proc, xp) = request.getfixturevalue(request.param)

    cfg = proc.config
    cfg.samples_per_record = samples_per_record
    cfg.ascans_per_block = ascans_per_block
    cfg.enable_log10 = False
    cfg.enable_ifft = False

    xp.random.seed(1234)
    spectra = xp.random.randint(0, 32767, cfg.input_shape).astype(xp.uint16)
    ascans = xp.random.randint(-128, 127, cfg.output_shape).astype(xp.int8)

    return (xp, proc, cfg, spectra, ascans)

def test_copy(scenario):
    (xp, proc, cfg, spectra, ascans) = scenario

    proc.initialize(cfg)

    proc.next(spectra, ascans)

    ref = spectra.astype(float)**2
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

def test_ifft(scenario):
    (xp, proc, cfg, spectra, ascans) = scenario

    cfg.enable_ifft = True
    proc.initialize(cfg)

    proc.next(spectra, ascans)

    ref = xp.abs(xp.fft.ifft(spectra.astype(float), axis=1))**2
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

def test_log10(scenario):
    (xp, proc, cfg, spectra, ascans) = scenario

    cfg.enable_log10 = True
    proc.initialize(cfg)

    proc.next(spectra, ascans)

    ref = 20 * xp.log10(spectra.astype(float) + xp.finfo(float).eps)
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

if __name__ == '__main__':
    pytest.main()
